import logging
import threading
from enum import Enum

from flask import Flask, redirect, render_template, session


class Player(object):
    def __init__(self, name):
        self.name = name

    def play(self, panel):
        panel.select(self)


class NonePlayer(Player):
    _instance = None
    _lock = threading.Lock()

    def __new__(cls):
        with cls._lock:
            if cls._instance is None:
                cls._instance = super().__init__(cls, 'nobody')
        return cls._instance


class Panel(object):
    def __init__(self, panel_id):
        self._id = panel_id
        self._player = NonePlayer()
    
    @property
    def panel_id(self):
        return self._id

    @property
    def player(self):
        return self._player

    @property
    def is_selected(self):
        return self._player is not NonePlayer()

    def select(self, player):
        if self.is_selected:
            raise Exception(f'Already selected: {self.panel_id}')
        self._player = player


class PlayBoard(object):
    ROWS = 3
    COLUMNS = 3
    _WIN_SET = (
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8},  # rows
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},  # columns
            {0, 4, 8}, {2, 4, 6},             # cross
    )

    def __init__(self):
        self._panels = [ Panel(i) for i in range(self.ROWS * self.COLUMNS) ]

    @property
    def panels(self):
        return self._panels

    def get_panel(self, panel_id):
        return self._panels[panel_id]

    def judge(self, player):
        player_panels = filter(lambda p: p.player == player, self.panels)
        panel_ids = set([p.panel_id for p in player_panels])
        return any(map(lambda win: win <= panel_ids, self._WIN_SET))

    def has_next(self):
        return not all(map(lambda p: p.is_selected, self.panels))


class GameStatus(Enum):
    NEW = 'new'
    PROCESS = 'process'
    SETTLEMENT = 'settlement'
    DONE = 'done'


class ThreeInaRow(object):
    def __init__(self):
        self._board = PlayBoard()
        self._players = [Player('player0'), Player('player1')]
        self._history = []
        self._winner = None
        self._status = GameStatus.NEW
    
    @property
    def board(self):
        return self._board
    
    @property
    def status(self):
        return self._status

    @property
    def is_over(self):
        return self.status == GameStatus.SETTLEMENT or self.status == GameStatus.DONE

    @property
    def history(self):
        return self._history
    
    @property
    def winner(self):
        return self._winner

    def get_current_player(self):
        return self._get_player(len(self._history))

    def _get_player(self, turn):
        i = turn % len(self._players)
        return self._players[i]

    def validate(self, panel_id):
        if not isinstance(panel_id, int):
            raise Exception(f'panel_id is invalid: {panel_id}')
        for panel in self.board.panels:
            if panel_id == panel.panel_id:
                return panel
        raise Exception(f'panel_id is invalid: {panel_id}')

    def play(self, panel):
        player = self.get_current_player()
        player.play(panel)
        self._history.append(panel)
        if self.board.judge(player):
            self._winner = player
            self._status = GameStatus.SETTLEMENT
        elif self.board.has_next():
            self._status = GameStatus.PROCESS
        else:
            self._status = GameStatus.DONE


class SessionManager(object):
    def __init__(self, session):
        selected_ids = session.get('selected_ids')
        if selected_ids is None or not isinstance(selected_ids, list):
            selected_ids = []
            session['selected_ids'] = selected_ids
        self._selected_ids = selected_ids
        self._game = self._load_game(selected_ids)
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self._save_game()

    def get_game(self) -> ThreeInaRow:
        return self._game

    def _save_game(self):
        session['selected_ids'] = [p.panel_id for p in self._game.history]

    def _load_game(self, selected_ids):
        game = ThreeInaRow()
        for selected_id in selected_ids:
            panel = game.board.get_panel(selected_id)
            game.play(panel)
        return game

app = Flask(__name__)
app.secret_key = 'aaa'

@app.route("/")
def index():
    return render_template(
        'index.html',
        game_status=GameStatus,
        game=SessionManager(session).get_game()
    )

@app.route("/<int:clicked_id>")
def clicked(clicked_id):
    try:
        with SessionManager(session) as session_manager:
            game = session_manager.get_game()
            select_panel = game.validate(clicked_id)
            game.play(select_panel)
    except Exception as e:
        logging.warning(f'[ERROR]clicked: {clicked_id}', exc_info=True)
        return redirect('/')

    return render_template(
        'index.html',
        game_status=GameStatus,
        game=game,
    )

@app.route("/new")
def new():
    session.clear()
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True)
